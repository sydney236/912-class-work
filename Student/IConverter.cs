﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Student
{
    /// <summary>
    /// Преобразователь данных
    /// </summary>
    public interface IConverter<T>
    {
        /// <summary>
        /// Преобразовать данные
        /// </summary>
        /// <param name="arg">данные</param>
        /// <returns></returns>
        T Convert(T arg);
    }
}
