﻿using System;

namespace Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            var s1 = new Student() { City = "Kazan" };
            var s2 = new Student() { City = "Kazan" };
            Console.WriteLine(s1.Equals(s2));

            var runner = new LinkedRunner();
            runner.Run();
        }

        public class Student { 
            public string City { get; set; }
            public string Fio { get; set; }
            public DateTime Birthday { get; set; }
        }
    }
}
