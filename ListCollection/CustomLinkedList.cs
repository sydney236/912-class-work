﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ListCollection
{
    /// <summary>
    /// Двунаправленный связный список
    /// </summary>
    public class CustomLinkedList<T> : ICustomCollection<T>
        where T : IComparable<T>
    {
        private LinkedNode<T> head;

        ///<inheritdoc/>
        public void Add(T item)
        {
            if (IsEmpty())
            {
                head = new LinkedNode<T>() { Data = item };
            }
            else
            {
                var node = head;
                while(node.NextNode!=null)
                {
                    node = node.NextNode;
                }
                var newNode = new LinkedNode<T>() { Data = item, PrevNode = node };
                node.NextNode = newNode;
            }
        }
        ///<inheritdoc/>
        public void AddRange(T[] items)
        {
            if (items.Length == 0) return;
            for(int i=0; i<items.Length; i++)
            {
                this.Add(items[i]);
            }
        }
        ///<inheritdoc/>
        public void Clear()
        {
            head = null;
        }
        ///<inheritdoc/>
        public bool Contains(T data)
        {
            LinkedNode<T> node = head;

            while (node != null)
            {
                if (node.Data.CompareTo(data) == 0)
                    return true;
                node = node.NextNode;
            }

            return false;
        }
        ///<inheritdoc/>
        public int IndexOf(T item)
        {
            int i = 0;
            LinkedNode<T> node = head;
            while (node != null)
            {
                if (node.Data.CompareTo(item) == 0)
                    return i;
                else node = node.NextNode;
                i++;
            }
            return -1; //это если нужного элемента нет в списке
        }
        ///<inheritdoc/>
        public void Insert(int index, T item)
        {
            if (index < 0 || index > Size()) throw new IndexOutOfRangeException();
            if (index == Size()) //если index на 1 больше, чем индекс последнего элемента (до вызова Insert'а)
            {
                this.Add(item);
                return;
            }
            if (index == 0)
            {
                LinkedNode<T> Node = new LinkedNode<T> { Data = head.Data, NextNode = head.NextNode};
                head.NextNode.PrevNode = Node;
                //далее сдвигаем головной элемент
                head.NextNode = Node;
                head.Data = item;
                Node.PrevNode = head;
                return;
            }
            LinkedNode<T> node = head;
            for (int i = 0; i < index - 1; i++) //идём до элемента, который стоит перед нужной позицией
            {
                node = node.NextNode;
            }
            LinkedNode<T> ShiftedNode = new LinkedNode<T> { Data = node.NextNode.Data, NextNode = node.NextNode.NextNode };
            LinkedNode <T> NewNode = new LinkedNode<T> { Data = item, NextNode = ShiftedNode, PrevNode=node };
            node.NextNode = NewNode;
            ShiftedNode.PrevNode = NewNode;
        }
        ///<inheritdoc/>
        public bool IsEmpty()
        {
            return head == null;
        }
        ///<inheritdoc/>
        public void Remove(T item)
        {
            //проверка списка на пустоту
            if (IsEmpty())
            {
                return;
            }

            //Значение - головной элемент
            if (head.Data.CompareTo(item) == 0)
            {
                head.NextNode.PrevNode = null;
                head = head.NextNode;
                return;
            }

            //идем до конца списка, пока не встретим нужный элемент
            LinkedNode<T> node = head;
            while (node.NextNode != null && node.NextNode.Data.CompareTo(item) != 0)
            {
                node = node.NextNode;
            }
            if (node.NextNode != null)
            {
                node.NextNode = node.NextNode.NextNode;
                if (node.NextNode != null) node.NextNode.PrevNode = node;
            }
        }
        ///<inheritdoc/>
        public void RemoveAll(T item)
        {
            while (this.Contains(item)) Remove(item);
        }
        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            //проверка на выход за границы коллекции
            if (index < 0 || index > (Size() - 1))
                throw new IndexOutOfRangeException();


            //Удаление первого элемента
            if (index == 0)
            {
                head = head.NextNode;
                head.PrevNode = null;
            }
            else
            {
                var node = head;
                for (var i = 0; i < index-1; i++) {
                    node = node.NextNode;
                }
                node.NextNode = node.NextNode.NextNode;
                if(node.NextNode!=null ) node.NextNode.PrevNode = node;
            }
        }
        ///<inheritdoc/>
        public void Reverse()
        {
            if (Size() == 0 || Size() == 1) return;
            LinkedNode<T> LeftNode = null; //текущий элемент
            LinkedNode<T> RightNode = null; //отзеркаленный элемент
            int Stop = Size() / 2 - 1; //это индекс текущего элемента
            while (Stop != -1)
            {
                LeftNode = head;
                //Stop будет уменьшаться при каждом выполнении из цикла for, так что текущий элемент будет постепенно сдвигаться влево
                for (int i = 0; i < Stop; i++)
                {
                    LeftNode = LeftNode.NextNode;
                }
                Stop--;
                if (RightNode == null)
                {
                    if (Size() % 2 == 0) RightNode = LeftNode.NextNode;
                    else RightNode = LeftNode.NextNode.NextNode; //если размер нечетный, то "перепрыгиваем" середину, т.к она не изменится
                }
                T temp = RightNode.Data;
                RightNode.Data = LeftNode.Data;
                LeftNode.Data = temp;
                RightNode = RightNode.NextNode;
            }
        }
        ///<inheritdoc/>
        public int Size()
        {
            if (head == null) return 0;
            LinkedNode<T> node = head;
            int size = 0;
            while (node.NextNode != null)
            {
                size++;
                node = node.NextNode;
            }
            return size + 1;
        }


        public void Display()
        {
            if (!IsEmpty())
            {
                LinkedNode<T> node = head;
                while (node != null)
                {
                    Console.Write(node.Data + " ");
                    node = node.NextNode;
                }

                Console.WriteLine();
            }
            else Console.WriteLine("List is empty");
        }


        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return new LinkedNodeEnumerator<T>(head);
        }

        public IEnumerator GetEnumerator()
        {
            return new LinkedNodeEnumerator<T>(head);
        }

        public bool EqualData(ICustomCollection<T> collection)
        {
            var list = (CustomLinkedList<T>)collection;
            var node = head;
            var listHead = list.head;

            if (list.Size() != Size()) return false;
            while (node != null)
            {
                if (node.Data.CompareTo(listHead.Data) != 0) return false;

                node = node.NextNode;
                listHead = listHead.NextNode;
            }
            return true;
        }

    }


    public class LinkedNodeEnumerator<T> : IEnumerator<T>
    {
        private LinkedNode<T> head;
        private LinkedNode<T> current;
        public LinkedNodeEnumerator(LinkedNode<T> head)
        {
            this.head = head;
            current = new LinkedNode<T>() { Data = default(T), NextNode = head };
        }

        public bool MoveNext()
        {
            if (current.NextNode != null)
            {
                current = current.NextNode;
                return true;
            }

            return false;
        }
        T IEnumerator<T>.Current => current.Data;
        object IEnumerator.Current => current.Data;
        public void Reset()
        {
            current = head;
        }

        public void Dispose()
        {
        }
    }
}
