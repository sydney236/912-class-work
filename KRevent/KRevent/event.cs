﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace KRevent
{
    public class FireEventArgs : EventArgs
    {
        public string ForestName;
        public FireEventArgs(string s)
        {
            ForestName = s;
        }
    }
    public class Forest
    {
        public event EventHandler<FireEventArgs> Burning;
        public string Name;
        public int Square { get; set; }
        public Forest(int square, string name)
        {
            Square = square;
            Name = name;
        }
        public void Run()
        {
            var fargs = new FireEventArgs(this.Name);
            Burning?.Invoke(this, fargs);
        }
    }
    public class FiremanTeam
    {
        private string Name { get; set; }
        public FiremanTeam(string name)
        {
            Name = name;
        }
        public void Subscribe(Forest forest)
        {
            forest.Burning += FiremanMission;
        }
        public void Unsubscribe(Forest forest)
        {
            forest.Burning -= FiremanMission;
        }
        private void FiremanMission(object sender, FireEventArgs e)
        {
            Console.WriteLine($"Пожарники выехали тушить пожар в лесу под названием {((Forest)sender).Name}.");
        }
    }
    public class MChS
    {
        private string City { get; set; }
        public MChS(string city)
        {
            City = city;
        }
        public void Subscribe(Forest forest)
        {
            forest.Burning += MChSMission;
        }
        public void Unsubscribe(Forest forest)
        {
            forest.Burning -= MChSMission;
        }
        private void MChSMission(object sender, FireEventArgs e)
        {
            Console.WriteLine($"МЧС в городе {City} производит эвакуацию населённых пунктов " +
                $"в связи с пожаром леса под названием {e.ForestName}.");
        }
    }
}
