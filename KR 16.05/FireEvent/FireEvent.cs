﻿using System;

namespace FireEvent
{
    public class Forest
    {
        public delegate void FireHandler();
        public event FireHandler Burning = FIsBurning;
        private string Name = "Горкинско-Ометьевский";
        public int Square 
        { 
            get { return Square; }
            set { Square = 66; }
        }
        public Forest(int square, string name)
        {
            Square = square;
            Name = name;
        }
        public void Run()
        {
            Burning?.Invoke();
        }
        public static void FIsBurning()
        {
            Console.WriteLine("Лес горит!");
        }
        public static void Display()
        {
            Console.WriteLine($"Применяем рефлексию.");
        }
    }
    public class FiremanTeam
    {
        private string Name { get; set; }
        public FiremanTeam(string name)
        {
            Name = name;
        }
        public void Subscribe(Forest forest)
        {
            forest.Burning += FiremanMission;
        }
        public void Unsubscribe(Forest forest)
        {
            forest.Burning -= FiremanMission;
        }
        private static void FiremanMission()
        {
            Console.WriteLine("Пожарники выехали тушить пожар.");
        }
    }
    public class MChS
    {
        private string City { get; set; }
        public MChS(string city)
        {
            City = city;
        }
        public void Subscribe(Forest forest)
        {
            forest.Burning += MChSMission;
        }
        public void Unsubscribe(Forest forest)
        {
            forest.Burning -= MChSMission;
        }
        private static void MChSMission()
        {
            Console.WriteLine("МЧС производит эвакуацию населённых пунктов.");
        }
    }
}
