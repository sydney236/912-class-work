﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Student
{
    /// <summary>
    /// операция по модулю 7
    /// </summary>
    public class IntConverter2 : IConverter<int>
    {
        public int Convert(int arg)
        {
            return arg % 7;
        }
    }
}
