﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace events2
{
    class Program
    {
        static void Main(string[] args)
        {
            Runner.RunStation();
            Runner.RunDropdown();
        }
    }
}
