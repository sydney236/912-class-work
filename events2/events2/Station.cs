﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace events2
{
    public class Station
    {
        public string Name;
        public int Temp;
        public readonly int MaxTemp = 400;

        public Action<Station> ReachedMax;

        public void Work()
        {
            bool Working = true;
            while (Working)
            {
                Console.WriteLine("Current temperature: " + Temp);
                Temp += 30;
                if (Temp >= MaxTemp && ReachedMax != null)
                {
                    ReachedMax(this);
                    Working = false;
                }
            }
        }
    }
}
