﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Student;
using ListCollection;

namespace TrueRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("проверка дз, заданного 22.02.20\n\nОДНОСВЯЗНЫЙ СПИСОК:");
            CustomList<int> list1 = new CustomList<int>();
            list1.Add(7);
            list1.Add(-4);
            list1.AddRange(new int[3] { 1, 3, 2 });
            list1.Display();
            if (!list1.Contains(8)) Console.WriteLine("В списке нет 8");
            if (list1.Contains(1)) Console.WriteLine($"Индекс первой единицы равен {list1.IndexOf(1)}");
            list1.Remove(6);
            list1.Remove(3);
            list1.AddRange(new int[2] { 1, 1 });
            list1.Display();
            list1.RemoveAll(1);
            list1.Display();
            list1.RemoveAt(1);
            list1.Display();
            list1.AddRange(new int[3] { 999, 0, 324 });
            list1.Reverse();
            list1.Display();
            list1.Clear();
            list1.Display();

            Console.WriteLine("\nДВУСВЯЗНЫЙ СПИСОК:");
            CustomLinkedList<int> list2 = new CustomLinkedList<int>();
            list2.Add(7);
            list2.Add(-4);
            list2.AddRange(new int[3] { 1, 3, 2 });
            list2.Display();
            if (!list2.Contains(8)) Console.WriteLine("В списке нет 8");
            if (list2.Contains(1)) Console.WriteLine($"Индекс первой единицы равен {list2.IndexOf(1)}");
            list2.Remove(6);
            list2.Remove(3);
            list2.AddRange(new int[2] { 1, 1 });
            list2.Display();
            list2.RemoveAll(1);
            list2.Display();
            list2.RemoveAt(1);
            list2.Display();
            list2.AddRange(new int[3] { 999, 0, 324 });
            list2.Reverse();
            list2.Display();
            list2.Clear();
            list2.Display();



            Console.WriteLine("\n\nпроверка дз, заданного 29.02.20\n\nARRAYLIST:\n");


            ArrayList<int> list3 = new ArrayList<int>();
            list3.Add(7);
            list3.Add(-4);
            list3.AddRange(new int[3] { 1, 3, 2 });
            list3.Display();
            if (!list3.Contains(8)) Console.WriteLine("В списке нет 8");
            Console.WriteLine(list3.Size());
            if (list3.Contains(1)) Console.WriteLine($"Индекс первой единицы равен {list3.IndexOf(1)}");
            list3.Remove(6);
            list3.Display();
            Console.WriteLine(list3.Size());
            list3.Remove(3);
            list3.AddRange(new int[2] { 1, 1 });
            list3.Display();
            list3.RemoveAll(1);
            list3.Display();
            list3.RemoveAt(1);
            list3.Display();
            list3.AddRange(new int[3] { 999, 0, 324 });
            list3.Reverse();
            list3.Display();
            list3.Clear();
            list3.Display();

            Console.WriteLine("\nПЕРЕЧИСЛЕНИЯ:");

            list1.AddRange(new int[3] { 3, 6, 1 });
            list2.AddRange(new int[3] { 3, 6, 2 });
            list3.AddRange(new int[3] { 3, 6, 3 });

            Console.WriteLine("Элементы из первого спика:");
            foreach (var item in list1)
            {
                Console.Write(item+ " ");
            }
            Console.WriteLine("\nЭлементы из второго спика:");
            foreach (var item in list2)
            {
                Console.Write(item+ " ");
            }
            Console.WriteLine("\nЭлементы из третьего спика:");
            foreach (var item in list3)
            {
                Console.Write(item+ " ");
            }
            Console.WriteLine();



            CustomList<int> list1eq = new CustomList<int>();
            list1eq.AddRange(new int[3] { 3, 6, 1 });
            if(list1.EqualData(list1eq)) { Console.WriteLine("Списки совпадают"); }

            CustomList<int> list1neq = new CustomList<int>();
            list1neq.AddRange(new int[3] { 3, 1, 1 });
            if (!list1.EqualData(list1neq)) { Console.WriteLine("Списки не совпадают"); }


            CustomLinkedList<int> list2eq = new CustomLinkedList<int>();
            list2eq.AddRange(new int[3] { 3, 6, 2 });
            if (list2.EqualData(list2eq)) { Console.WriteLine("Списки совпадают"); }

            CustomLinkedList<int> list2neq = new CustomLinkedList<int>();
            list2neq.AddRange(new int[3] { 9, 1, 1 });
            if (!list2.EqualData(list2neq)) { Console.WriteLine("Списки не совпадают"); }


            ArrayList<int> list3eq = new ArrayList<int>();
            list3eq.AddRange(new int[3] { 3, 6, 3 });
            if (list3.EqualData(list3eq)) { Console.WriteLine("Списки совпадают"); }

            ArrayList<int> list3neq = new ArrayList<int>();
            list3neq.AddRange(new int[3] { 11, 1, 1 });
            if (!list3.EqualData(list3neq)) { Console.WriteLine("Списки не совпадают"); }
        }
    }
}
