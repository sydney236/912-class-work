﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace coin
{

    class Program
    {
        // curr... содержит все слагаемые для текущей комбинации
        static void Run(int sum, int[] coins, List<string> allvariants, List<int> currentVariant)
        {
            List<string> variants;
            if (sum < 0) return;
            else
            {
                if (sum == 0)
                {
                    currentVariant.Sort();
                    var s = String.Join("+", currentVariant.ToArray());
                    if (allvariants.Contains(s) == false)
                    {
                        allvariants.Add(s);
                    }
                    return;
                }

                foreach (var item in coins)
                {
                    if (sum >= item)
                    {
                        var newVariant = currentVariant.ToList();
                        newVariant.Add(item);
                        Run(sum - item, coins, allvariants, newVariant);
                    }
                }
            }
        }

        public static void Main(string[] args)
        {
            //здесь будут храниться все возможные варианты
            List<string> allvars = new List<string>();
            int[] coins = new int[] { 1, 2, 5, 10 };
            Console.Write("Enter sum ");
            int sum = Convert.ToInt32(Console.ReadLine());

            Run(sum, coins, allvars, new List<int>());

            Console.WriteLine("Result");
            foreach (var item in allvars)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        } 
    }
}
