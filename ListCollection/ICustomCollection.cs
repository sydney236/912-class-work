﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ListCollection
{
    /// <summary>
    /// кастомная коллекция
    /// </summary>
    public interface ICustomCollection<T> : IEnumerable<T>
    {
        /// <summary>
        /// Размер коллекции
        /// </summary>
        int Size();

        /// <summary>
        /// Пустой ли список
        /// </summary>
        bool IsEmpty();

        /// <summary>
        /// Содержится ли элемент с таким значением в коллекции
        /// </summary>
        /// <returns></returns>
        bool Contains(T data);

        /// <summary>
        /// Добавить элемент в конец коллеции
        /// </summary>
        void Add(T item);

        /// <summary>
        /// Добавить несколько элементов в конец коллеции
        /// </summary>
        void AddRange(T[] items);

        /// <summary>
        /// Удаляет первый встретившийся элемент с конкретным значением
        /// </summary>
        void Remove(T item);

        /// <summary>
        /// Удаляет все элементы с конкретным зачением
        /// </summary>
        void RemoveAll(T item);

        /// <summary>
        /// удаление эелемнта на позиции
        /// </summary>
        /// <param name="index"></param>
        void RemoveAt(int index);

        /// <summary>
        /// Очищение списка
        /// </summary>
        void Clear();

        /// <summary>
        /// Перевернуть коллекцию
        /// </summary>
        void Reverse();

        /// <summary>
        /// Вставить на позицию Index значение item
        /// </summary>
        /// <param name="i">позиция</param>
        /// <param name="item">значение</param>
        void Insert(int index, T item);

        /// <summary>
        /// Возвращает позицию первого элемента со значением T
        /// </summary>
        int IndexOf(T item);
    }
}
