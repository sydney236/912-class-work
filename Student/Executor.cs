﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Student
{
    /// <summary>
    /// Класс-исполнитель
    /// </summary>
    public class Executor<T>
    {
        /// <summary>
        /// Выполнить 
        /// </summary>
        /// <param name="input">Входные данные</param>
        /// <param name="dataConvertor">Преобразователь данных</param>
        /// <param name="output">Вывод данных</param>
        public void Execute(T input, Func<T, T> dataConvertor,
            Action<T> output)
        {
            var convertedData = dataConvertor(input);

            output(convertedData);
        }
    }
}
