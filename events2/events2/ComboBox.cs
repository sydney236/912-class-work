﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace events2
{
    public class ComboBox
    {
        public int Value;
        public int[] states = new[] { 0, 1, 2 };
        public string[] labels = new string[] { "11-912", "11-911", "11-910" };

        public event Action<ComboBox> Changing;

        public void Change(int newValue)
        {
            if (!states.Contains(newValue)) return;

            Value = newValue;
            Console.WriteLine(labels[Value]);
            if (Changing != null)
                Changing(this);
        }
    }
}
