﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Student;


namespace ListTest
{
    [TestClass]
    public class DelegateTest
    {
        [TestMethod]
        public void RunDelegateExample()
        {
            var e = new DelegateExamples();
            e.Run();
        }

        [TestMethod]
        public void RunExecuter() 
        {
            //класс-исполнитель для целых чисел
            var executor1 = new Executor<int>();
            var data1 = 5;
            var dataConverter1 = new Func<int, int>(x => x + 1);
            var dataOutput1 = new Action<int>(x => Console.WriteLine(x));

            executor1.Execute(data1, dataConverter1, dataOutput1);

            var executor2 = new Executor<string>();
            executor2.Execute("hello",
                x => x + ", my friend",
                x => Console.WriteLine(x.ToUpper()));
        }
    }
}
