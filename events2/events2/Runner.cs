﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace events2
{
    public class Runner
    {
        public static void RunDropdown()
        {
            ComboBox groups = new ComboBox();
            Table table = new Table();
            Label label = new Label();
            table.Register(groups);
            label.Register(groups);
            groups.Change(1);
        }

        public static void RunStation()
        {
            Station station = new Station();
            station.Name = "Leningrad nuclear plant";
            station.Temp = 150;
            Resque resque = new Resque();
            resque.Register(station);
            station.Work();
        }
    }
}
