﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tree;

namespace TreeRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            GTree<int> t = new GTree<int>(6);
            t.AddChild(7);
            t.AddChild(9);
            TreeTraversal<int> tt = Display;
            t.Traverse(t, tt);
        }
        static void Display(string data)
        {
            Console.WriteLine($"'{data}'");
        }
    }
}
