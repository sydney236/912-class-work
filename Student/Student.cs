﻿using System;

namespace Student
{
    /// <summary>
    /// Студент
    /// </summary>
    public class Student: IComparable<Student>
    {
        public string City { get; set; }
        public string Fio { get; set; }
        public DateTime Birthday { get; set; }
        public decimal Mark { get; set; }


        public int CompareTo(Student other)
        {
            if (City.CompareTo(other.City) == 0)
            {
                if (Fio.CompareTo(other.Fio) == 0)
                {
                    if (Birthday.CompareTo(other.Birthday) == 0)
                    {
                        return Mark.CompareTo(other.Mark);
                    }
                    else return Birthday.CompareTo(other.Birthday);
                }
                else return Fio.CompareTo(other.Fio);
            }
            else return City.CompareTo(other.City);
        }
    }
}
