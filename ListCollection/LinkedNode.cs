﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ListCollection
{
    /// <summary>
    /// Элемент двунаправленного линейного списка
    /// </summary>
    public class LinkedNode<T>
    {
        /// <summary>
        /// Ссылка на предыдущий элемент
        /// </summary>
        public LinkedNode<T> PrevNode { get; set; }

        /// <summary>
        /// Ссылка на следующий элемент
        /// </summary>
        public LinkedNode<T> NextNode { get; set; }

        /// <summary>
        /// Поле, хранящее информацию
        /// </summary>
        public T Data { get; set; }
    }
}
