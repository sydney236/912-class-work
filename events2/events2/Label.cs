﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace events2
{
    public class Label
    {
        private string[] states = new[] { "Label for 11-912", "Label for 11-911", "Label for 11-910" };
        public void Register(ComboBox box)
        {
            box.Changing += Change;
        }

        public void Change(ComboBox box)
        {
            int value = box.Value;
            Console.WriteLine(states[value]);
        }
    }
}
