﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ListCollection;

namespace Runner
{
    /// <summary>
    /// Класс для запуска кода 
    /// </summary>
    public class Runner
    {
        /*public void Run()
        {
            try
            {
                var node = LineListUtils.ReadLineList("List1.txt");
               // LineListUtils.WriteLineList(node);

               // LineListUtils.Check(node);


                LineListUtils.WriteLineList(LineListUtils.DeleteNodeWithGivenValue(null,5));
                LineListUtils.WriteLineList(LineListUtils.DeleteNodeWithGivenValue(node, 666));
                LineListUtils.WriteLineList(LineListUtils.DeleteNodeWithGivenValue(node, 3));
                LineListUtils.WriteLineList(LineListUtils.DeleteNodeWithGivenValue(node, 13));
                LineListUtils.WriteLineList(LineListUtils.DeleteNodeWithGivenValue(node, 2));
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
            }
            catch (IOException)
            {
                Console.WriteLine("Ошибка ввода-вывода");
            }
            catch (Exception ex) {
                Console.WriteLine("Ошибка приложения " + ex.Message);
                throw ex;
            }
        }
        */
        public void Run()
        {
            CustomList<int> list = new CustomList<int>();
            list.Add(5);
            list.Add(6);
            list.Add(7);
            list.Add(8);

            list.Display();

            list.Remove(5);
            list.Display();

            list.Remove(7);
            list.Display();

            list.Remove(8);
            list.Display();

            Console.WriteLine($"Is there 235 in the list? - {list.Contains(235)}");
            Console.WriteLine($"Is there 6 in the list? - {list.Contains(6)}");
        }
    }
}
