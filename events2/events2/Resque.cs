﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace events2
{
    public class Resque
    {
        public string Name;
        public void Register(Station station)
        {
            station.ReachedMax += Extinguish;
        }

        public void Extinguish(Station station)
        {
            Console.WriteLine("Extinguishing station " + station.Name);
        }
    }
}
