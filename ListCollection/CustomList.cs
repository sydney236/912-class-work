﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;

namespace ListCollection
{
    /// <summary>
    /// Линейный односвязный список
    /// </summary>
    public class CustomList<T> : ICustomCollection<T> where T : IComparable<T>
    {
        private Node<T> head;
        ///<inheritdoc/>
        public void Add(T item)
        {
            if (IsEmpty()) head = new Node<T>() { Data = item };
            else
            {
                Node<T> node = head;
                while (node.NextNode != null) node = node.NextNode;
                node.NextNode = new Node<T>() { Data = item };
            }
        }
        ///<inheritdoc/>
        public void AddRange(T[] items)
        {
            if (items.Length == 0)
            {
                return;
            }
            for(int i=0; i<items.Length; i++)
            {
                this.Add(items[i]);
            }


            //удалить, если все эти трюки сверху работают без проблем
            //
            //if (items.Length == 0)
            //{
            //    return;
            //}
            //if (IsEmpty())
            //{
            //    head = new Node<T>() { Data = items[0] };
            //    if (items.Length > 1)
            //    {
            //        Node<T> node = head;
            //        for (int i = 1; i < items.Length; i++)
            //        {
            //            node.NextNode = new Node<T>() { Data = items[i] };
            //            node = node.NextNode;
            //        }
            //    }
            //}
            //else
            //{
            //    Node<T> node = head;
            //    while (node.NextNode != null) node = node.NextNode;
            //    for (int i = 0; i < items.Length; i++)
            //    {
            //        node.NextNode = new Node<T>() { Data = items[i] };
            //        node = node.NextNode;
            //    }
            //}
        }
        ///<inheritdoc/>
        public void Clear()
        {
            head = null;
        }
        ///<inheritdoc/>
        public bool Contains(T data)
        {
            Node<T> curNode = head;

            while (curNode != null)
                if (curNode.Data.CompareTo(data) == 0)
                    return true;
                else
                    curNode = curNode.NextNode;

            return false;
        }
        ///<inheritdoc/>
        public int IndexOf(T item)
        {
            int i = 0;
            Node<T> node = head;
            while (node != null)
            {
                if (node.Data.CompareTo(item) == 0)
                    return i;
                else node = node.NextNode;
                i++;
            }
            return -1; //если элемента нет в списке
        }
        ///<inheritdoc/>
        public void Insert(int index, T item)
        {
            if (index < 0 || index > Size()) throw new IndexOutOfRangeException();
            if (index == Size()) //если index на 1 больше, чем индекс последнего элемента (до вызова Insert'а)
            {
                this.Add(item);
                return;
            }
            if (index == 0)
            {
                Node<T> Node = new Node<T> {Data = head.Data, NextNode = head.NextNode };
                //далее сдвигаем головной элемент
                head.NextNode = Node; 
                head.Data = item;
                return;
            }
            Node<T> node = head;
            for (int i = 0; i < index - 1; i++) //идём до элемента, который стоит перед нужной позицией
            {
                node = node.NextNode;
            }
            Node<T> ShiftedNode = new Node<T> { Data = node.NextNode.Data, NextNode = node.NextNode.NextNode };
            node.NextNode = new Node<T> { Data = item, NextNode = ShiftedNode };
        }
        ///<inheritdoc/>
        public bool IsEmpty()
        {
            return head == null;
        }
        ///<inheritdoc/>
        public void Remove(T item)
        {
            //проверка списка на пустоту
            if (IsEmpty())
            {
                return;
            }

            //Значение - головной элемент
            if (head.Data.CompareTo(item) == 0)
            {
                head = head.NextNode;
                return;
            }

            //идем до конца списка, пока не встретим нужный элемент
            Node<T> node = head;
            while (node.NextNode != null
                && node.NextNode.Data.CompareTo(item) != 0)
            {
                node = node.NextNode;
            }

            if (node.NextNode != null)
            {
                node.NextNode = node.NextNode.NextNode;
            }
        }
        ///<inheritdoc/>
        public void RemoveAll(T item)
        {
            //проверка списка на пустоту
            if (IsEmpty())
            {
                return;
            }

            //Удаляю с головы занчения item
            while (head.Data.CompareTo(item) == 0)
            {
                head = head.NextNode;
                if (head == null) return;
            }

            RemoveFromElement(head, item);
        }

        private void RemoveFromElement(Node<T> node, T item)
        {
            if (node.NextNode == null)
                return;
            if (node.NextNode.Data.CompareTo(item) != 0)
            {
                node = node.NextNode;
            }
            else
            {
                node.NextNode = node.NextNode.NextNode;
            }
            RemoveFromElement(node, item);
        }

        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            if (index < 0 || index >= Size()) throw new IndexOutOfRangeException();
            if (index == 0)
            {
                head = head.NextNode;
            }
            else
            {
                Node<T> node = head;
                for (int i = 0; i < index - 1; i++) //идём до элемента, который стоит перед нужной позицией
                {
                    node = node.NextNode;
                }
                node.NextNode = node.NextNode.NextNode;
            }
        }
        ///<inheritdoc/>
        public void Reverse()
        {
            if (Size() == 0 || Size() == 1) return;
            /* 
             * сначала берём элемент из левой части ("текущий" элемент), который ближе всего к середине (но не стоит в середине)
             * меняем местами значение текущего элемента и значение элемента на "отзеркаленной" позиции
             * изначально, "отзеркаленная" позиция == позиция эл-та из правой части, стоящего ближе всего к середине (но не в самой середине)
             * постепенно будем приближать текущий элемент к "голове", параллельно двигая "отзеркаленную" позицию направо
             * когда поменям местами первый и последний элементы, то всё, конец метода
             */
            Node<T> LeftNode = null; //текущий элемент
            Node<T> RightNode = null; //отзеркаленный элемент
            int Stop = Size() / 2 - 1; //это индекс текущего элемента
            while (Stop != -1)
            {
                LeftNode = head;
                //Stop будет уменьшаться при каждом выполнении из цикла for, так что текущий элемент будет постепенно сдвигаться влево
                for (int i = 0; i < Stop; i++)
                {
                    LeftNode = LeftNode.NextNode;
                }
                Stop--;
                if (RightNode == null)
                {
                    if (Size() % 2 == 0) RightNode = LeftNode.NextNode;
                    else RightNode = LeftNode.NextNode.NextNode; //если размер нечетный, то "перепрыгиваем" середину, т.к она не изменится
                }
                //далее меняем местами текущий и отзеркаленный элемент
                T temp = RightNode.Data;
                RightNode.Data = LeftNode.Data;
                LeftNode.Data = temp;
                RightNode = RightNode.NextNode; 
            }
        }
        ///<inheritdoc/>
        ///<inheritdoc/>
        public void Display()
        {
            if (!IsEmpty())
            {
                Node<T> node = head;
                while (node != null)
                {
                    Console.Write(node.Data + " ");
                    node = node.NextNode;
                }

                Console.WriteLine();
            }
            else Console.WriteLine("List is empty");
        }
        public int Size()
        {
            if (head == null) return 0;
            Node<T> node = head;
            int size = 0;
            while (node.NextNode != null)
            {
                size++;
                node = node.NextNode;
            }
            return size + 1;
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return new NodeEnumerator<T>(head);
        }

        public IEnumerator GetEnumerator()
        {
            return new NodeEnumerator<T>(head);
        }

        public bool EqualData(ICustomCollection<T> collection)
        {
            var list = (CustomList<T>)collection;
            var node = head;
            var listHead = list.head;

            if (list.Size() != Size()) return false;
            while (node != null)
            {
                if (node.Data.CompareTo(listHead.Data) != 0) return false;

                node = node.NextNode;
                listHead = listHead.NextNode;
            }
            return true;
        }
    }

    public class NodeEnumerator<T> : IEnumerator<T>
    {
        private Node<T> head;
        private Node<T> current;

        public NodeEnumerator(Node<T> head)
        {
            this.head = head;
            current = new Node<T>() { Data = default(T), NextNode = head };
        }

        public bool MoveNext()
        {
            if (current.NextNode != null)
            {
                current = current.NextNode;
                return true;
            }

            return false;
        }

        T IEnumerator<T>.Current => current.Data;
        object IEnumerator.Current => current.Data;
        public void Reset()
        {
            current = head;
        }

        public void Dispose()
        {
        }
    }
}
