﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ListCollection
{
    /// <summary>
    /// Коллекция на основе массива
    /// </summary>
    public class ArrayList<T> : IEnumerable<T>, ICustomCollection<T>
        where T : IComparable<T>
    {
        T[] mas;
        /// <summary>
        /// Сколько ячеек с информацией в массиве
        /// </summary>
        int count = 0; 

        public void Add(T item)
        {
            if (IsEmpty())
            {
                mas = new T[1] { item };
                count = 1;
            }
            else 
            {
                if (count == mas.Length)
                {
                    var newMas = new T[mas.Length + 1];
                    for (int i = 0; i < mas.Length; i++)
                    {
                        newMas[i] = mas[i];
                    }
                    mas = newMas;
                }

                mas[count] = item;
                count++;
            }
        }

        public void AddRange(T[] items)
        {
            foreach (var item in items)
            {
                Add(item);
            }
        }

        public void Clear()
        {
            count = 0;
            mas = new T[1];
        }

        public bool Contains(T data)
        {
            //индексоф возвращает -1, если эл-та нет
            return IndexOf(data) != -1;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new ArrayEnumerator<T>(mas);
        }

        public int IndexOf(T item)
        {
            for (int i = 0; i < Size(); i++)
            {
                if (mas[i].CompareTo(item) == 0) return i;
            }

            return -1;
        }

        public void Insert(int index, T item)
        {
            if (index < 0 || index > count) throw new IndexOutOfRangeException();
            T[] mas1 = new T[count + 1];
            for (int i=0; i<index; i++)
            {
                mas1[i] = mas[i];
            }
            mas1[index] = item;
            for(int i=index; i<count; i++)
            {
                mas1[i+1] = mas[i];
            }
            mas = mas1;
            count++;
        }

        public bool IsEmpty()
        {
            return count == 0;
        }

        public void Remove(T item)
        {
            if(Contains(item))
            {
                T[] mas1 = new T[count - 1];
                int index = IndexOf(item);
                for(int i =0; i<index; i++)
                {
                    mas1[i] = mas[i];
                }
                for(int i=index; i<count-1; i++)
                {
                    mas1[i] = mas[i + 1];
                }
                count--;
                mas = mas1;
            }
        }

        public void RemoveAll(T item)
        {
            while(Contains(item))
            {
                Remove(item);
            }
        }

        public void RemoveAt(int index)
        {
            if(index<0 || index>=count) throw new IndexOutOfRangeException();
            T[] mas1 = new T[count - 1];
            for (int i = 0; i < index; i++)
            {
                mas1[i] = mas[i];
            }
            for (int i = index; i < count - 1; i++)
            {
                mas1[i] = mas[i + 1];
            }
            count--;
            mas = mas1;
        }

        public void Reverse()
        {
            for (int i = 0; i < count / 2; i++)
            {
                var tmp = mas[count - 1 - i];
                mas[count - 1 - i] = mas[i];
                mas[i] = tmp;
            }
        }

        public int Size()
        {
            return count;
        }

        public override string ToString() 
        {
            string result = string.Empty;
            for (int i = 0; i < count; i++) {
                result += " " + mas[i].ToString();
            }
            return result;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


        public bool EqualData(ICustomCollection<T> collection)
        {
            collection = (ArrayList<T>)collection;
            if (Size() != collection.Size()) return false;
            int i = 0;
            foreach (var item in collection)
            {
                if (item.CompareTo(mas[i]) != 0) return false;
                i++;
            }
            return true;
        }

        public void Display()
        {
            foreach(var item in mas)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }
    }

    public class ArrayEnumerator<T> : IEnumerator<T>
    {
        private T[] mas;
        private int position = -1;

        public void Dispose()
        {
        }

        public ArrayEnumerator(T[] mas)
        {
            this.mas = mas;
        }

        public T Current
        {
            get
            {
                //
                if (position == -1 || position >= mas.Length)
                    throw new IndexOutOfRangeException();
                return mas[position];
            }
        }
        object IEnumerator.Current => Current;
        public bool MoveNext()
        {
            if (position < mas.Length - 1)
            {
                position++;
                return true;
            }
            else
                return false;
        }

        public void Reset()
        {
            position = -1;
        }

    }
}
