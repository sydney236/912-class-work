﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ListCollection;

namespace ListTest
{
    [TestClass]
    public class CustomListTest
    {
        [TestMethod]
        public void IsEmptyTest()
        {
            var list = new CustomList<int>();
            Assert.IsTrue(list.IsEmpty());
        }

        [TestMethod]
        public void ForeachTest()
        {
            var list = new CustomList<int>();
            list.Add(5);
            list.Add(7);
            list.Add(31);
            list.Add(5);
            list.Add(95);
            foreach (var node in list) {
                var i = node;
            }
        }
        [TestMethod]
        public void RemoveAllTest() 
        {
            var list = new CustomList<int>() { 5,5,5,3,7,5,6,5,5,9,18,5,5};
            list.RemoveAll(5);//3,7,6,9,18

        }
    }
}
