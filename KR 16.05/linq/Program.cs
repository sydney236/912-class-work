﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace linq
{

    public class Linq
    {
        public class Product
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        public class Price
        {
            public int Id { get; set; }
            public int ProductId { get; set; }
            public decimal Sum { get; set; }
            public bool IsActual { get; set; }
        }

        public class Position //позиция в счете
        {
            public int ProductId;
            public decimal Price;
            public int Count;

            public Position(int productId, List<Price> prices)
            {
                ProductId = productId;
                Price = prices
                    .Where(p => p.IsActual)
                    .Where(p => p.ProductId == ProductId)
                    .Select(p => p.Sum)
                    .First();
                Count = 1;
            }
        }

        public class Invoice
        {
            public List<Position> Positions;

            public Invoice(List<Price> prices, int[] productIds)
            {
                Positions = new List<Position>();
                foreach (var productId in productIds)
                {
                    var newPosition = new Position(productId, prices);
                    if (Positions.Where(p => p.ProductId == productId).Count() != 0)
                    {
                        Positions
                            .Where(pos => pos.ProductId == productId)
                            .First().Count++;
                        continue;
                    }
                    Positions.Add(newPosition);
                }
            }

            public void Show(List<Product> products)
            {
                foreach (var position in Positions)
                {
                    var name = products
                        .Where(product => product.Id == position.ProductId)
                        .First().Name;
                    Console.WriteLine(@"Name: {0}, Count: {1}, Price: {2}", name, position.Count, position.Price);
                }
            }
        }

        public class Promoution
        {
            public List<Tuple<int, int>> PromoPositions;
            public int DiscountProcent;
            public decimal DiscountSum;

            public Promoution(int discount, List<Price> prices, int[] productIds)
            {
                DiscountProcent = discount;
                Array.Sort(productIds);
                PromoPositions = new List<Tuple<int, int>>();
                var lastId = productIds[0];
                var count = 1;

                foreach (var productId in productIds)
                {
                    DiscountSum += prices
                            .Where(p => p.IsActual)
                            .Where(p => p.ProductId == productId)
                            .Select(p => p.Sum)
                            .First() * DiscountProcent / 100;
                }

                for (int i = 1; i < productIds.Length; i++)
                {
                    count++;
                    if (i + 1 == productIds.Length)
                    {
                        PromoPositions.Add(Tuple.Create(productIds[i - 1], count));
                    }
                    if (productIds[i] == lastId)
                    {
                        continue;
                    }
                    else
                    {
                        PromoPositions.Add(Tuple.Create(productIds[i - 1], count));
                        count = 0;
                    }
                }
            }

            public int Search(Invoice invoice)
            {
                var count = int.MaxValue;
                foreach (var position in PromoPositions)
                {
                    var promoPositions = invoice.Positions
                        .Where(pos => pos.ProductId == position.Item1);
                    Position promoPosition;
                    if (promoPositions.Count() == 0)
                    {
                        return 0;
                    }
                    else
                    {
                        promoPosition = promoPositions.First();
                    }
                    if (promoPositions == null) return 0;
                    count = Math.Min(count, promoPosition.Count / position.Item2);
                }
                return count;
            }

            public void Show(List<Product> products)
            {
                foreach (var e in PromoPositions)
                {
                    var name = products
                        .Where(product => product.Id == e.Item1)
                        .First().Name;
                    Console.WriteLine(@"Name: {0} * {1} = {2}% discount", name, e.Item2, DiscountProcent);
                }
            }
        }

        public static void PurchaseSum(List<Price> prices, List<Promoution> promoutions, int[] productIds)
        {
            var invoice = new Invoice(prices, productIds);
            var sum = invoice.Positions
                .Select(position => position.Count * position.Price)
                .Sum();

            decimal maxDiscount = 0;
            foreach (var promoution in promoutions)
            {
                var count = promoution.Search(invoice);
                maxDiscount = Math.Max(maxDiscount, promoution.DiscountSum * count);
            }

            Console.WriteLine(@"Sum: {0}, With Discount: {1}", sum, sum - maxDiscount);
        }

        public static void Run()
        {
            var products = new List<Product>
            {
                new Product { Id = 1, Name = "Аквариум 10 литров" },
                new Product { Id = 2, Name = "Аквариум 20 литров" },
                new Product { Id = 3, Name = "Аквариум 50 литров" },
                new Product { Id = 4, Name = "Аквариум 100 литров" },
                new Product { Id = 5, Name = "Аквариум 200 литров" },
                new Product { Id = 6, Name = "Фильтр" },
                new Product { Id = 7, Name = "Термометр" }
            };

            var prices = new List<Price>
            {
                new Price { Id = 1, ProductId = 1, Sum = 100, IsActual = false },
                new Price { Id = 2, ProductId = 1, Sum = 123, IsActual = true },
                new Price { Id = 3, ProductId = 2, Sum = 234, IsActual = true },
                new Price { Id = 4, ProductId = 3, Sum = 532, IsActual = true },
                new Price { Id = 5, ProductId = 4, Sum = 234, IsActual = true },
                new Price { Id = 6, ProductId = 5, Sum = 534, IsActual = true },
                new Price { Id = 7, ProductId = 5, Sum = 124, IsActual = false },
                new Price { Id = 8, ProductId = 6, Sum = 153, IsActual = true },
                new Price { Id = 9, ProductId = 7, Sum = 157, IsActual = true }
            };

            //создание списка счетов
            var invoiceList = new List<Invoice>();
            invoiceList.Add(new Invoice(prices, new int[] { 1, 1, 4, 5, 7 }));
            invoiceList.Add(new Invoice(prices, new int[] { 3, 5, 7, 5, 7 }));
            invoiceList.Add(new Invoice(prices, new int[] { 2, 2, 2 }));


            //демонстрация счета
            invoiceList[0].Show(products);
            Console.WriteLine();

            //средняя цена для каждого товара
            var averagePrices = prices
                .GroupBy(p => p.ProductId)
                .Select(g => Tuple.Create(g.Key, g.Average(e => e.Sum)));

            foreach (var averagePrice in averagePrices)
            {
                Console.WriteLine(@"ProductId: {0} Price: {1}", averagePrice.Item1, averagePrice.Item2);
            }
            Console.WriteLine();

            //список акций
            var promoutionList = new List<Promoution>();
            promoutionList.Add(new Promoution(10, prices, new int[] { 1, 1 }));
            promoutionList.Add(new Promoution(15, prices, new int[] { 1, 5 }));
            promoutionList.Add(new Promoution(20, prices, new int[] { 3, 4 }));

            //содержание акции
            promoutionList[0].Show(products);
            Console.WriteLine();

            //обработка заказа
            PurchaseSum(prices, promoutionList, new int[] { 1, 1, 5, 6, 7 });

            /*Задания *
            * 1) создать список счетов (один счет содержит несколько пар цена-количество)
            * например, один счет - это аквариум на 200 литров, два фильтра и термометр
            * 2) вывести счет для покупателя с колонками "Наименование услуги, сумма, итого"
            * в порядке сортировки по наименованию товара в лексикографическом порядке
            * - 1 балл
            * 3) Вывести среднюю цену для каждого продукта с учетом неактуальных значений
            * - 1 балл
            * 4) создать список акций (код продукта, скидка):
            * аквариум на 200 литров + 2 фильтра - скидка 15%
            * аквариум 100 литров + 2 фильтра - 10% скидка
            * любой другой аквариум + фильтр - 5% скидка
            * 5) создать список перечень всех названий товаров в группе акции +
            * цена до скидки + цена с учетом скидки - 1 балл
            *
            * Для тех, кто выбрал вариант посложнее: написать функцию подсчета
            * суммы покупки (выявлять, есть ли в наборе продуктов акционные комплекты,
            * при их наличии делать скидку),
            * выводить итого без учета скидки и со скидкой, отельно сумму скидки
            * - это + 2 балла
            * использовать Linq операции со множествами
            */
        }
    }

    class Program
    {
        public static void Main()
        {
            Linq.Run();
        }
    }
}

