﻿using System;
using System.Collections.Generic;

namespace Tree
{
    public delegate void TreeTraversal<T>(string nodeData);

    public class GTree<T>
    {
        private T data;
        private LinkedList<GTree<T>> children;

        public GTree(T Data)
        {
            data = Data;
            children = new LinkedList<GTree<T>>();
        }

        public void AddChild(T data)
        {
            children.AddFirst(new GTree<T>(data));
        }

        public void Traverse(GTree<T> node, TreeTraversal<T> tt)
        {
            if(tt!=null) tt(node.data.ToString());
            foreach (GTree<T> kid in node.children)
                Traverse(kid, tt);
        }
    }
}
