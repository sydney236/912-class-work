﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Student
{
    /// <summary>
    /// Добавление префикса
    /// </summary>
    public class StringConverter1 : IConverter<string>
    {
        public string Convert(string arg)
        {
            return "Гарифуллина " + arg;
        }
    }
}
