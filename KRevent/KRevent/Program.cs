﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KRevent
{
    public class Program
    {
        static void Main(string[] args)
        {
            Forest forest1 = new Forest(999, "Горкинско-Ометьевский");
            FiremanTeam fteam1 = new FiremanTeam("Команда пожарников номер 423");
            MChS m1 = new MChS("Казань");
            m1.Subscribe(forest1);
            fteam1.Subscribe(forest1);
            forest1.Run();
        }
    }
}
